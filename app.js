// app.js
App({
  onLaunch() {
    this.readToken();
    this.readServeUrl();
  },
  globalData: {
    tokenKey: '__access_token__',
    serveUrlKey: '__serve_url_key__',
    accessToken: ''
  },
  readToken () {
    wx.getStorage({
      key: this.globalData.tokenKey,
      success: res => {
        if (res) this.globalData.accessToken = res;
      }
    });
  },
  saveToken (token) {
    wx.setStorage({
      data: token,
      key: this.globalData.tokenKey,
    });
  },
  readServeUrl () {
    return new Promise((resolve, reject) => {
      wx.getStorage({
        key: this.globalData.serveUrlKey,
        success (res) { resolve(res) }
      });
    });
  },
  saveServeUrl (url) {
    wx.setStorage({
      data: url,
      key: this.globalData.serveUrlKey
    });
  },
  getAccessToken () {
    return new Promise((resolve, reject) => {
      if (this.globalData.accessToken) return resolve(this.globalData.accessToken);
      let url = 'https://aip.baidubce.com/oauth/2.0/token';
      url += '?grant_type=client_credentials';
      url += '&client_id=mni0tUc7Gjt7k0xPRVW0P8oG';
      url += '&client_secret=6k7OcYmcRR9zhDL6IWPdsM3GZWPEnLFH';
      wx.request({
        url,
        method: 'GET',
        success: res => {
          const { access_token, error, error_description } = res.data;
          if (error) return reject(error_description);
          this.globalData.accessToken = access_token;
          this.saveToken(access_token);
          resolve(access_token);
        },
        fail (res) {
          reject(res);
        }
      });
    });
  },
  doOCR (imagePath) {
    return new Promise((resolve, reject) => {
      const fs = wx.getFileSystemManager();
      const base64Str = fs.readFileSync(imagePath, 'base64');
      let url = 'https://aip.baidubce.com/rest/2.0/ocr/v1/accurate_basic';
      url += '?access_token=' + this.globalData.accessToken;
      wx.showLoading({
        title: '正在识别 . . .',
      });
      wx.request({
        url,
        method: 'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        data: {
          image: base64Str,
          paragraph: true
        },
        success: res => {
          resolve(res);
        },
        fail (e) {
          reject(e);
        }
      })
    });
  },
  findPC () {
    wx.onLocalServiceFound((result) => {
      console.log(result);
    });
    wx.onLocalServiceLost((result) => {
      console.log(result);
    });
    wx.startLocalServiceDiscovery({
      serviceType: '_http._tcp.',
      success: res => {
        console.log(res)
      },
      fail: e => {
        console.log(e)
      },
      complete: () => console.log('complete')
    })
  }
})
