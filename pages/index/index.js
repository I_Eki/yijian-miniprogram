// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    initFailed: false,
    imagePath: '',
    resultText: '',
    result: null,
    pcServeUrl: 'http://192.168.0.102:830',
    pcIP: 'http://192.168.0.102',
    pcServePort: 830,
    hasPC: false
  },
  onLoad() {
    app.getAccessToken().catch(e => {
      wx.showModal({
        content: '初始化百度API所需AccessToken失败，请与作者联系！',
        showCancel: false,
        success: res => {
          this.setData({
            initFailed: true
          });
        },
        fail (e) {
          console.log(e)
        }
      })
    });
    // app.findPC();
    // app.findPC().then(res => {
    //   this.setData({
    //     hasPC: true
    //   });
    // });
  },
  // 拍照并调取 OCR 识别服务
  takePhoto () {
    const ctx = wx.createCameraContext();
    ctx.takePhoto({
      quality: 'high',
      success: res => {
        this.setData({
          imagePath: res.tempImagePath
        });
        app.doOCR(res.tempImagePath).then(res => {
          wx.hideLoading();
          const { words_result } = res.data;
          this.setData({
            result: res.data,
            resultText: words_result.map(i => i.words).join('')
          });
        }).catch(e => {
          wx.showToast({
            title: e,
          })
        });
      }
    });
  },
  reset () {
    this.setData({
      imagePath: '',
      resultText: '',
      result: null
    });
  },
  sendToPC () {
    wx.showLoading({
      title: '正在发送至PC端 . . .',
    });
    const { pcIP, pcServePort, result, pcServeUrl } = this.data;
    wx.request({
      // url: pcIP + ':' + pcServePort + '/translate',
      url: pcServeUrl + '/translate',
      method: 'POST',
      data: result,
      success (res) {
        wx.hideLoading();
        wx.showToast({
          title: '已发送到PC！',
        });
        app.saveServeUrl(pcServeUrl);
      }
    });
  },
  inputHandle (e) {
    const { target: { dataset: { key } }, detail: { value } } = e;
    this.setData({
      [key]: value
    });
  },
  error(e) {
    console.log(e.detail);
  }
})
